package com.example.hai_pc.assignment1;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void onCalculateClick(View a) {


        if (a.getId() == R.id.buttonCalculate) {
            EditText a1 = (EditText) findViewById(R.id.loanTextField);
            EditText a2 = (EditText) findViewById(R.id.interestTextField);

            double l, i;
            l = Double.parseDouble(a1.getText().toString());
            i = Double.parseDouble(a2.getText().toString());


            double monthlyInterest = i*0.01/12;
            double power5Year = Math.pow(1+monthlyInterest,-5*12);
            double result5Year = (monthlyInterest*l/(1-power5Year));

            double power10Year = Math.pow(1+monthlyInterest,-10*12);
            double result10Year = (monthlyInterest*l/(1-power10Year));

            double power15Year = Math.pow(1+monthlyInterest,-15*12);
            double result15Year = (monthlyInterest*l/(1-power15Year));

            double power20Year = Math.pow(1+monthlyInterest,-20*12);
            double result20Year = (monthlyInterest*l/(1-power20Year));

            double power25Year = Math.pow(1+monthlyInterest,-25*12);
            double result25Year = (monthlyInterest*l/(1-power25Year));

            double power30Year = Math.pow(1+monthlyInterest,-30*12);
            double result30Year = (monthlyInterest*l/(1-power30Year));


            TextView t5 = (TextView)findViewById(R.id.VBox1);
            TextView t10 = (TextView)findViewById(R.id.VBox2);
            TextView t15 = (TextView)findViewById(R.id.VBox3);
            TextView t20 = (TextView)findViewById(R.id.VBox4);
            TextView t25 = (TextView)findViewById(R.id.VBox5);
            TextView t30 = (TextView)findViewById(R.id.Vbox6);
             t5.setText("5  years   : "+String.format("%.2f",result5Year));
            t10.setText("10 years  : "+String.format("%.2f",result10Year));
            t15.setText("15 years  : "+String.format("%.2f",result15Year));
            t20.setText("20 years  : "+String.format("%.2f",result20Year));
            t25.setText("25 years  : "+String.format("%.2f",result25Year));
            t30.setText("30 years  : "+String.format("%.2f",result30Year));

        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
